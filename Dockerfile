FROM node:8-alpine

RUN npm install express

RUN npm install request

RUN npm install dotenv

RUN npm install mongodb

COPY env.docker.v2 .env

COPY kube-db-app.js .

CMD node kube-db-app.js >> ./logs/kube-db-app.out

EXPOSE 3002
